//
//  ViewController.swift
//  edurekamodule9b
//
//  Created by Jay on 02/03/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit



class ViewController: UIViewController
{


    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //lets setup the plist
        //already created a file in the project folder called 'data.plist'.
        //if you dont create a file called data.plist, the app wont run.

        //here is the setup where we are connecting our plist file to the Documents folder
        //to which the app does have permission to write.
        
        print("viewDidLoad has started")
        
        print("opening the plist file and writing operations began. ")
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = paths.appendingPathComponent("data.plist")
        let plistArray = NSMutableArray(contentsOfFile: path)
        if (plistArray != nil)
        {
            print("adding items to array.")
            plistArray?.add("Hello There")
            plistArray?.add("Hello There")
            plistArray?.add("Hello There")
            plistArray?.add("Hello There")
            
            print("writing the array modifications to the plist file began")
            plistArray?.write(toFile: path, atomically: false)
            print("writing the array modifications to the plist file ended")
        }
        else
        {
            print("something went wrong with NSMutableArray(contentsOfFile: path)")
        }
        
        print("opening the plist file and writing operations ended. ")
        
        print("opening the plist file and reading operations began. ")
        
        let rootPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, .userDomainMask, true)[0]
        let plistPathInDocument = rootPath.appendingPathComponent("data.plist")
        let result = FileManager.default.fileExists(atPath: plistPathInDocument)
        if !result
        {
            let plistPathInBundle = Bundle.main.path(forResource: "data", ofType: "plist") as String?
            do
            {
                try FileManager.default.copyItem(atPath: plistPathInBundle!, toPath: plistPathInDocument)
                print("copying plist was successfull")
            }
            catch
            {
                print("Error occurred while copying file to document \(error)")
            }
        }
        else
        {
            print("plist already exists in Documents folder of app ")

        }
        
         print("opening the plist file and reading operations ended. ")
        print("viewDidLoad has stopped")

    }
    
    @IBAction func addItemsToPlist()
    {
        print("addItemsToPlist began. ")
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = paths.appendingPathComponent("data.plist")
        let plistArray = NSMutableArray(contentsOfFile: path)
        if (plistArray != nil)
        {
            print("adding items to array.")
            plistArray?.add("Hello There addItemsToPlist")
            
            print("writing the array modifications to the plist file began")
            plistArray?.write(toFile: path, atomically: false)
            print("writing the array modifications to the plist file ended")
        }
        else
        {
            print("something went wrong with NSMutableArray(contentsOfFile: path)")
        }
        
        print("addItemsToPlist ended. ")
    }
    
    @IBAction func showItemsToPlist()
    {
        print("showItemsToPlist began. ")
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = paths.appendingPathComponent("data.plist")
        let plistArray = NSMutableArray(contentsOfFile: path)
        if (plistArray != nil)
        {
            print(plistArray!)
        }
        else
        {
            print("something went wrong with NSMutableArray(contentsOfFile: path)")
        }
        
        print("showItemsToPlist ended. ")
    }
    
    @IBAction func clearItemsToPlist()
    {
        print("clearItemsToPlist began. ")
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = paths.appendingPathComponent("data.plist")
        
        //get an empty array
        let myArray:NSMutableArray = [0]
        //write it to the plist file, essentially making it empty.
        myArray.write(toFile: path, atomically: true)
        
        print("showItemsToPlist ended. ")
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//I need this extension because Apple once again changed how String works.
//why does apple keep doing this with every new version of swift.
//for dear god sake.
extension String
{
    func appendingPathComponent(_ string: String) -> String
    {
        print("did some appendingPathComponent stuff")
        return URL(fileURLWithPath: self).appendingPathComponent(string).path
    }
}

